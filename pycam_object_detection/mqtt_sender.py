# Brandon Tan 14/4/21

import paho.mqtt.client as mqtt
import time
import json

# emqx broker for testing, put public ipv4 address for real test
mqttBroker = "broker.emqx.io"
#mqttBroker = "192.168.10.233"

f = open("message_share.txt", "r")
MQTT_MSG = f.read()

print(MQTT_MSG)

def on_publish(client, userdata, mid):
    print("Message published...")
    
def on_connect(client, userdata, flags, rc):
    if rc==0:
        print("Successfully connected rc: ", str(rc))
    else:
        print("Failed to connect rc: ", str(rc))
    
client = mqtt.Client("Sensor_Device_1")
client.on_connect = on_connect
client.on_publish = on_publish
client.username_pw_set(username="mqtt", password="password")
client.connect(mqttBroker, 1883, 45)
client.loop_start()
while True:
    f = open("message_share.txt", "r")
    MQTT_MSG = f.read()
    f.close()
    client.publish("UOM/Outside/Camera", MQTT_MSG)
    time.sleep(120) # publish only every 2 minute
